package atl.grade.change;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import atl.grade.Demo;

/**
 *
 * @author jlc
 */
public class DemoInsert extends Demo {

    @Override
    public void execute(String url) {
        try {
            Connection connexion = DriverManager.getConnection("jdbc:sqlite:" + url);
            Statement stmt = connexion.createStatement();

            String query = "INSERT INTO Lessons (acronym) values('ANLL')";

            int count = stmt.executeUpdate(query);
            System.out.println("\t Nombre de cours modifié : " + count);

            ResultSet result = stmt.getGeneratedKeys();
            while (result.next()) {
                String acronym = result.getString(1);
                System.out.println("\t clé ajoutée : " + acronym);
            }
        } catch (SQLException ex) {
            System.out.println("DEMO_INSERT | Erreur " + ex.getMessage() + " SQLState " + ex.getSQLState());
        }
    }

    @Override
    public String getTitle() {
        return "Insertion d'un utilisateur dans la DB";
    }
}
