package atl.grade.date;

import atl.grade.Demo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class DemoDateInsert extends Demo {

    @Override
    public void execute(String url) {
        try {
            Connection connexion = DriverManager.getConnection("jdbc:sqlite:" + url);
            Statement stmt = connexion.createStatement();

            String query = "INSERT INTO GRADES (score,date,dateModified,id_student,id_lesson)"+ "values(7 , '2020-04-02' , '2020-05-02 10:27:33' ,1, 'SYS')";

            int count = stmt.executeUpdate(query);
            System.out.println("\t Nombre de cours modifié : " + count);

            query = "SELECT * FROM GRADES";

            ResultSet result = stmt.executeQuery(query);
//            ResultSet result = stmt.getGeneratedKeys();

            while (result.next()) {
                int id = result.getInt("score");
                String dateText = result.getString("date");
                String modifiedText = result.getString("dateModified");
                int idStudent = result.getInt("id_Student");
                String idLesson = result.getString("id_lesson");
                System.out.println("\t record : " + id + " " + dateText + " " + modifiedText + " " + idStudent + " " + idLesson);
            }
        } catch (SQLException ex) {
            System.out.println("DEMO_DATE_SELECT | Erreur " + ex.getMessage() + " SQLState " + ex.getSQLState());
        }
    }

    @Override
    public String getTitle() {
        return "Insertion d'un utilisateur dans la DB";
    }
}
