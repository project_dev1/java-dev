package atl.exercice;

import atl.grade.Demo;
import atl.grade.config.ConfigManager;
import atl.grade.date.DemoDateSelect;
import java.io.IOException;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class Ex1 {

       public static void main(String[] args) {
        try {
            ConfigManager.getInstance().load();
            String dbUrl = ConfigManager.getInstance().getProperties("db.url");
            System.out.println("Base de données stockée : " + dbUrl);

            Demo demo = new DemoDateSelect();
            demo.printTitle();
            demo.execute(dbUrl);
        } catch (IOException ex) {
            System.out.println("Erreur IO " + ex.getMessage());
        }
    }
}
