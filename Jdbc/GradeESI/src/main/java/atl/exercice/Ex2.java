package atl.exercice;

import atl.grade.Demo;
import atl.grade.change.DemoDelete;
import atl.grade.config.ConfigManager;
import java.io.IOException;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class Ex2 {

       public static void main(String[] args) {
        try {
            ConfigManager.getInstance().load();
            String dbUrl = ConfigManager.getInstance().getProperties("db.url");
            System.out.println("Base de données stockée : " + dbUrl);

            Demo demo = new DemoDelete();
            demo.printTitle();
            demo.execute(dbUrl);
        } catch (IOException ex) {
            System.out.println("Erreur IO " + ex.getMessage());
        }
    }
}
