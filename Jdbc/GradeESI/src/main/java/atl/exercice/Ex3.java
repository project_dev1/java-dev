package atl.exercice;

import atl.grade.Demo;
import atl.grade.config.ConfigManager;
import atl.grade.date.DemoDateInsert;
import atl.grade.prepare.DemoPrepare;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class Ex3 {

    public static void main(String[] args) {
        try {
            ConfigManager.getInstance().load();
            String dbUrl = ConfigManager.getInstance().getProperties("db.url");
            System.out.println("Base de données stockée : " + dbUrl);

            Demo demo = new DemoDateInsert();
            demo.printTitle();
            demo.execute(dbUrl);
        } catch (IOException ex) {
            System.out.println("Erreur IO " + ex.getMessage());
        }
    }
 
}
