package Synchronisation;

/**
 *
 * @author Billal Zidi
 */
public class Test {

    public static void main(String[] args) {
        ToujoursPair tp = new ToujoursPair();
        MyThread t = new MyThread(tp);
        t.start();
        while (true) {
            tp.nextI();
            if (tp.getI() % 10000000 == 0) {
                System.out.println(tp.getI());
            }
        }
    }
}

