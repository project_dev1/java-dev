/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

/**
 *
 * @author Billal Zidi
 */
public class MyObject {

    private String name;

    public MyObject(String name) {
        this.name = name;
    }

//     public void show() {
    public synchronized void show() {
        String nom = Thread.currentThread().getName();
        System.out.println("My object: thread " + nom
                + ", objet " + name + " in show");
        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
        }
        System.out.println("My object: thread " + nom
                + ",objet " + name + " out show");
    }

//     public void print() {
    public synchronized void print() {
        String nom = Thread.currentThread().getName();
        System.out.println("My object: thread " + nom
                + ", objet " + name + " in print");
        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
        }
        System.out.println("My object: thread " + nom
                + ", objet " + name + " out print");
    }
}
