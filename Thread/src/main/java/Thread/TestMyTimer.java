package Thread;

/**
 *
 * @author Billal Zidi
 */
public class TestMyTimer {

    public static void main(String[] args) {
        MyTimer myTimer = new MyTimer(4000);
        myTimer.start();
        try {
            Thread.sleep(16000); //pendant 16 seconde tout le code en bas n'est pas pris en compte
        } catch (InterruptedException e) {
            System.out.println("TestMyTimer: exception " + e);
        }
        myTimer.shouldRun = false;
        System.gc();
        System.out.println("MyTimer: gc called");
        System.out.println("MyTimer: end");
    }

}
