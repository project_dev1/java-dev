package Thread;

/**
 *
 * @author Billal Zidi
 */
public class TestMyThreadComposition {

    public static void main(String[] args) {
        var co = new MyThreadComposition();
        co.start();
        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 5000000; ++j) ;
            System.out.println("MyThread: " + " : " + i);
        }
    }

}
