package Thread;

public class TestMyThread {

    public static void main(String[] args) {
        MyThread t = new MyThread("one");
        t.start();
//        t.run();//lorsqu'on ft appel à la methode run il y a pas de parallelisme 

        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 50000000; ++j) ;
            System.out.println("TestMyThread: " + i);
        }
    }

}
