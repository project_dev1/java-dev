package Thread;

public class MyTimer extends Thread {

    private int laps;
    public boolean shouldRun; // notez le public !

    public MyTimer(int laps) {
        this.laps = laps;
        shouldRun = true;
    }

    @Override
    public void run() {
        while (shouldRun) {
            try {
                sleep(laps / 2);
                System.out.println("MyTimer: run");
                sleep(laps / 2);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }

    }
}
