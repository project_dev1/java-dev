package Thread;

public class MyThreadComposition {

    private Thread t;

    public MyThreadComposition() {
        t = new Thread(() -> {
            for (int i = 0; i < 10; ++i) {
                for (int j = 0; j < 10000000; ++j) ;
                System.out.println("TestMyThreadComposition: " + i);
            }
        });
    }

    public void start() {
        t.start();
    }
}
