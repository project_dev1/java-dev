/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Thread;

import static java.lang.Thread.sleep;

/**
 *
 * @author Billal Zidi
 */
public class DaemonThread extends Thread {

    public static void main(String[] args) {
        DaemonThread d = new DaemonThread();
        d.setDaemon(true);
        d.start();
        try {
            System.out.println("DaemonThread main: i do nothing during a while");
            sleep(8000);
            d.join();
        } catch (InterruptedException e) {
            System.out.println("DaemonThread: exception " + e);
        }
    }

    @Override
    public void run() {
        for (int n = 0; n < 42; ++n) {
            System.out.println("DaemonThread: run " + n);
            try {
                sleep(400);
            } catch (InterruptedException e) {
                System.out.println("DaemonThread thread: exception " + e);
            }
        }
    }
}
