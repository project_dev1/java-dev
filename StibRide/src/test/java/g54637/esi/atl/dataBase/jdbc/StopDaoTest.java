package g54637.esi.atl.dataBase.jdbc;

import g54637.atl.esi.dataBase.configµ.ConfigManager;
import g54637.esi.atl.dataBase.dto.LinesDto;
import g54637.esi.atl.dataBase.dto.StationDto;
import g54637.esi.atl.dataBase.dto.StopDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class StopDaoTest {

    private StopDto parc;
    private StationDto stationPetillon;
    private LinesDto linePetillon;
    private Integer orderPetillon = 23;
    private final List<StopDto> all;
    private StopDao instance;

    public StopDaoTest() {
        System.out.println("==== StopDto Constructor =====");
        linePetillon = new LinesDto(5);
        stationPetillon = new StationDto(8212, "PETILLON");
        parc = new StopDto(linePetillon, stationPetillon, orderPetillon);
        try {
            ConfigManager.getInstance().load();
            instance = StopDao.getInstance();
        } catch (RepositoryException | IOException ex) {
            org.junit.jupiter.api.Assertions.fail("Erreur de connection à la base de données de test", ex);
        }

        all = new ArrayList<>();
        all.add(parc);
    }

    @Test
    public void testSelectExist() throws Exception {
        System.out.println("testSelectExistfirst");
        StopDto expected = parc;
        StopDto result = instance.select(8212);
        assertEquals(expected, result);
    }


    @Test
    public void selectAllStationInEachLines() throws Exception {
        System.out.println("testSelectExist");
        List<StopDto> expected = all;
        List<StopDto> result = instance.selectAllStationInEachLines(stationPetillon);
        assertEquals(expected, result);
    }

    @Test
    public void testSelectIncorrectParameter() throws Exception {
        System.out.println("testSelectIncorrectParameter");
        Integer incorrect = null;
        assertThrows(RepositoryException.class, () -> {
            instance.select(incorrect);
        });
    }
}
