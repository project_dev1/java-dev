package g54637.esi.atl.dataBase.repository;

import g54637.esi.atl.dataBase.dto.LinesDto;
import g54637.esi.atl.dataBase.dto.StationDto;
import g54637.esi.atl.dataBase.dto.StopDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import g54637.esi.atl.dataBase.jdbc.StopDao;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class StopRepositoryTest {

    @Mock
    private StopDao mock;

    private final StopDto petillon;
    private final StopDto churchill;
    private final StationDto stationPetillon;
    private final StationDto stationChurchill;
    private final LinesDto lineParc;
    private final LinesDto lineChurchill;
    private final Integer orderPetillon = 23;
    private final Integer orderChurchill = 100;
    private final List<StopDto> all;

    public StopRepositoryTest() {
        System.out.println("==== StopDto Constructor =====");
        lineParc = new LinesDto(5);
        lineChurchill = new LinesDto(10);

        stationPetillon = new StationDto(8212, "PETILLON");
        stationChurchill = new StationDto(2412, "CHURCHILL");

        petillon = new StopDto(lineParc, stationPetillon, orderPetillon);
        churchill = new StopDto(lineChurchill, stationChurchill, orderChurchill);
        all = new ArrayList<>();
        all.add(petillon);
        all.add(churchill);
    }

    @BeforeEach
    void init() throws RepositoryException {
        System.out.println("==== BEFORE EACH =====");
        Mockito.lenient().when(mock.select(stationPetillon.getKey())).thenReturn(petillon);
        Mockito.lenient().when(mock.select(churchill.getKey())).thenReturn(null);
        Mockito.lenient().when(mock.selectAll()).thenReturn(all);
        Mockito.lenient().when(mock.select(null)).thenThrow(RepositoryException.class);
    }

    @Test
    public void testGetExist() throws Exception {
        System.out.println("testGetExist");
        StopDto expected = petillon;
        StopRepository repository = new StopRepository(mock);
        StopDto result = repository.get(8212);
        assertEquals(expected, result);
        Mockito.verify(mock, times(1)).select(8212);
    }

    @Test
    public void testGetNotExist() throws Exception {
        System.out.println("testGetNotExist");

        StopRepository repository = new StopRepository(mock);

        StopDto result = repository.get(churchill.getKey());

        assertNull(result);
        Mockito.verify(mock, times(1)).select(churchill.getKey());
    }

    @Test
    public void testGetIncorrectParameter() throws Exception {
        System.out.println("testGetIncorrectParameter");
        StopRepository repository = new StopRepository(mock);
        Integer incorrect = null;
        assertThrows(RepositoryException.class, () -> {
            repository.get(incorrect);
        });
        Mockito.verify(mock, times(1)).select(null);
    }

    @Test
    public void testGetAll() throws Exception {
        System.out.println("testGetFullStudentExist");

        List<StopDto> expected = all;
        StopRepository repository = new StopRepository(mock);

        List<StopDto> result = repository.getAll();

        assertEquals(expected, result);
        Mockito.verify(mock, times(1)).selectAll();
    }
}
