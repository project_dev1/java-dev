package g54637.esi.atl.dataBase.jdbc;

import g54637.atl.esi.dataBase.configµ.ConfigManager;
import g54637.esi.atl.dataBase.dto.StationDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class StationsDaoTest {

    private final int KEY = 8032;
    private final StationDto parc;
    private final StationDto bervoets;
    private final StationDto gareDuNord;
    private final List<StationDto> all;

    private StationDao instance;

    public StationsDaoTest() {
        System.out.println("==== StationsDto Constructor =====");
        parc = new StationDto(KEY, "PARC");
        bervoets = new StationDto(99_999, "etg");
        gareDuNord = new StationDto(4, "gdn");
        all = new ArrayList<>();
        all.add(new StationDto(8012, "DE BROUCKERE"));
        all.add(new StationDto(8022, "GARE CENTRALE"));
        all.add(parc);
        all.add(new StationDto(8042, "ARTS-LOI"));
        all.add(new StationDto(8052, "MAELBEEK"));
        all.add(new StationDto(8062, "SCHUMAN"));
        all.add(new StationDto(8072, "MERODE"));
        all.add(new StationDto(8082, "MONTGOMERY"));
        all.add(new StationDto(8092, "JOSEPH.-CHARLOTTE"));
        all.add(new StationDto(8102, "GRIBAUMONT"));
        all.add(new StationDto(8112, "TOMBERG"));
        all.add(new StationDto(8122, "ROODEBEEK"));
        all.add(new StationDto(8132, "VANDERVELDE"));
        all.add(new StationDto(8142, "ALMA"));
        all.add(new StationDto(8152, "CRAINHEM"));
        all.add(new StationDto(8161, "STOCKEL"));
        all.add(new StationDto(8202, "THIEFFRY"));
        all.add(new StationDto(8212, "PETILLON"));
        all.add(new StationDto(8222, "HANKAR"));
        all.add(new StationDto(8232, "DELTA"));
        all.add(new StationDto(8242, "BEAULIEU"));
        all.add(new StationDto(8252, "DEMEY"));
        all.add(new StationDto(8262, "HERRMANN-DEBROUX"));
        all.add(new StationDto(8272, "SAINTE-CATHERINE"));
        all.add(new StationDto(8282, "COMTE DE FLANDRE"));
        all.add(new StationDto(8292, "ETANGS NOIRS"));
        all.add(new StationDto(8302, "TRONE"));
        all.add(new StationDto(8312, "PORTE DE NAMUR"));
        all.add(new StationDto(8322, "LOUISE"));
        all.add(new StationDto(8332, "HOTEL DES MONNAIES"));
        all.add(new StationDto(8342, "PORTE DE HAL"));
        all.add(new StationDto(8352, "GARE DU MIDI"));
        all.add(new StationDto(8362, "CLEMENCEAU"));
        all.add(new StationDto(8372, "DELACROIX"));
        all.add(new StationDto(8382, "GARE DE L''OUEST"));
        all.add(new StationDto(8412, "MADOU"));
        all.add(new StationDto(8422, "BOTANIQUE"));
        all.add(new StationDto(8432, "ROGIER"));
        all.add(new StationDto(8442, "YSER"));
        all.add(new StationDto(8462, "RIBAUCOURT"));
        all.add(new StationDto(8472, "ELISABETH"));
        all.add(new StationDto(8641, "ERASME"));
        all.add(new StationDto(8652, "EDDY MERCKX"));
        all.add(new StationDto(8662, "CERIA"));
        all.add(new StationDto(8672, "LA ROUE"));
        all.add(new StationDto(8682, "BIZET"));
        all.add(new StationDto(8692, "VEEWEYDE"));
        all.add(new StationDto(8702, "SAINT-GUIDON"));
        all.add(new StationDto(8712, "AUMALE"));
        all.add(new StationDto(8722, "JACQUES BREL"));
        all.add(new StationDto(8742, "BEEKKANT"));
        all.add(new StationDto(8754, "OSSEGHEM"));
        all.add(new StationDto(8764, "SIMONIS"));
        all.add(new StationDto(8774, "BELGICA"));
        all.add(new StationDto(8784, "PANNENHUIS"));
        all.add(new StationDto(8794, "BOCKSTAEL"));
        all.add(new StationDto(8804, "STUYVENBERGH"));
        all.add(new StationDto(8814, "HOUBA-BRUGMANN"));
        all.add(new StationDto(8824, "HEYSEL"));
        all.add(new StationDto(8833, "ROI BAUDOUIN"));
        try {
            ConfigManager.getInstance().load();
            instance = StationDao.getInstance();
        } catch (RepositoryException | IOException ex) {
            org.junit.jupiter.api.Assertions.fail("Erreur de connection à la base de données de test", ex);
        }
    }

    @Test
    public void testSelectExist() throws Exception {
        System.out.println("testSelectExist");
        //Arrange
        StationDto expected = parc;
        //Action
        StationDto result = instance.select(KEY);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSelectExist2() throws Exception {
        System.out.println("testSelectExist2");
        //Arrange
        StationDto expected = new StationDto(8042, "ARTS-LOI");
        //Action
        StationDto result = instance.select(8042);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSelectNotExist() throws Exception {
        System.out.println("testSelectNotExist");
        //Arrange
        //Action
        StationDto result = instance.select(bervoets.getKey());
        //Assert
        assertNull(result);
    }

    @Test
    public void testSelectNotExist2() throws Exception {
        System.out.println("testSelectNotExist");
        //Arrange
        //Action
        StationDto result = instance.select(gareDuNord.getKey());
        //Assert
        assertNull(result);
    }

    @Test
    public void testSelectAll() throws Exception {
        System.out.println("testSelectExist");
        //Arrange
        List<StationDto> expected = all;
        //Action
        List<StationDto> result = instance.selectAll();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSelectIncorrectParameter() throws Exception {
        System.out.println("testSelectIncorrectParameter");
        //Arrange
        Integer incorrect = null;
        //Assert
        assertThrows(RepositoryException.class, () -> {
            //Action
            instance.select(incorrect);
        });
    }

}
