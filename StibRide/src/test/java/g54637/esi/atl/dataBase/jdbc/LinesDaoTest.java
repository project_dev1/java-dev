package g54637.esi.atl.dataBase.jdbc;

import g54637.atl.esi.dataBase.configµ.ConfigManager;
import g54637.esi.atl.dataBase.dto.LinesDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class LinesDaoTest {

    private final int KEY = 1;
    private final LinesDto parc;
    private final LinesDto madou;
    private final LinesDto churchill;
    private final List<LinesDto> all;

    private LinesDao instance;

    public LinesDaoTest() {
        System.out.println("==== LinesDto Constructor =====");
        parc = new LinesDto(KEY);
        madou = new LinesDto(99_999);
        churchill = new LinesDto(3);
        all = new ArrayList<>();
        all.add(parc);
        all.add(new LinesDto(2));
        all.add(new LinesDto(5));
        all.add(new LinesDto(6));

        try {
            ConfigManager.getInstance().load();
            instance = LinesDao.getInstance();
        } catch (RepositoryException | IOException ex) {
            org.junit.jupiter.api.Assertions.fail("Erreur de connection à la base de données de test", ex);
        }
    }

    @Test
    public void testSelectExist() throws Exception {
        System.out.println("testSelectExist");
        //Arrange
        LinesDto expected = parc;
        //Action
        LinesDto result = instance.select(KEY);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSelectExist2() throws Exception {
        System.out.println("testSelectExist2");
        //Arrange
        LinesDto expected = new LinesDto(5);
        //Action
        LinesDto result = instance.select(5);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSelectNotExist() throws Exception {
        System.out.println("testSelectNotExist");
        //Arrange
        //Action
        LinesDto result = instance.select(madou.getKey());
        //Assert
        assertNull(result);
    }

    @Test
    public void testSelectNotExist2() throws Exception {
        System.out.println("testSelectNotExist2");
        //Arrange
        //Action
        LinesDto result = instance.select(churchill.getKey());
        //Assert
        assertNull(result);
    }

    @Test
    public void testSelectAll() throws Exception {
        System.out.println("testSelectAll");
        //Arrange
        List<LinesDto> expected = all;
        //Action
        List<LinesDto> result = instance.selectAll();
        //Assert
        assertEquals(expected, result);
    }

    @Test
    public void testSelectIncorrectParameter() throws Exception {
        System.out.println("testSelectIncorrectParameter");
        //Arrange
        Integer incorrect = null;
        //Assert
        assertThrows(RepositoryException.class, () -> {
            //Action
            instance.select(incorrect);
        });
    }

}
