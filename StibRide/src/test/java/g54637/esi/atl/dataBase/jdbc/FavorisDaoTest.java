package g54637.esi.atl.dataBase.jdbc;

import g54637.esi.atl.dataBase.dto.FavorisDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class FavorisDaoTest {

    private final FavorisDao instance;

    public FavorisDaoTest() throws SQLException, RepositoryException, IOException {
        System.out.println("==== FavorisDto Constructor =====");
        instance = FavorisDao.getInstance();
        instance.getConnexion().setAutoCommit(false);

    }

    @AfterEach
    public void tearDown() throws SQLException {
        instance.getConnexion().rollback();
    }

    /**
     * Test of select method, of class FavorisDao.
     */
    @Test
    public void testSelect() throws RepositoryException {
        System.out.println("testSelectExistfirst  ");
        FavorisDto test = new FavorisDto("New", "SquaresPants", "CHAHED");
        FavorisDto result = instance.select(test.getKey());
        assertEquals(test, result);
    }

    /**
     * Test of select method, of class FavorisDao.
     */
    @Test
    public void testSelectNotExist() throws Exception {
        System.out.println("testSelectNotExist");
        FavorisDto result = instance.select("hey");
        assertNull(result);
    }

    @Test
    public void testInsertNotExist() throws Exception {
        System.out.println("testInsertNotExist");
        FavorisDto fav = new FavorisDto("test", "hey0", "billal");
        instance.insert(fav);
        FavorisDto result = instance.select(fav.getKey());
        assertEquals(fav, result);
    }

    /**
     * Test of selectAll method, of class FavorisDao.
     */
    @Test
    public void testSelectAll() throws Exception {
        System.out.println("testSelectAll");
        List<FavorisDto> expected = new ArrayList<>();
        expected.add(new FavorisDto("New", "SquaresPants", "CHAHED"));
        List<FavorisDto> result = instance.selectAll();
        assertEquals(expected, result);

    }

    @Test
    public void testUpdateExist() throws Exception {
        FavorisDto parc = new FavorisDto("New", "test", "gdf");
        FavorisDto itemUpdated = new FavorisDto("New", "SquaresPants", "CHAHED");
        instance.update(parc, itemUpdated);
        var newSource = itemUpdated.getOrigin();
        var newDestination = itemUpdated.getDestination();
        FavorisDto result = instance.select(itemUpdated.getKey());
        assertEquals(newSource, result.getOrigin());
        assertEquals(newDestination, result.getDestination());

    }

    @Test
    public void testDeleteExist() throws Exception {
        FavorisDto itemUpdated = new FavorisDto("New", "SquaresPants", "CHAHED");
        instance.delete(itemUpdated.getKey());
        assertNull(instance.select(itemUpdated.getKey()));
    }

    @Test
    public void testDeleteNotExist() throws Exception {
        FavorisDto talent = new FavorisDto("ca", "c", "f");
        instance.delete(talent.getKey());
        assertNull(instance.select(talent.getKey()));

    }

    @Test
    public void testDeleteParameterIncorrect() throws Exception {
        String incorrect = null;
        assertThrows(RepositoryException.class,
                () -> {
                    instance.select(incorrect);
                }
        );
    }

}
