package g54637.esi.atl.dataBase.repository;

import g54637.esi.atl.dataBase.dto.FavorisDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import g54637.esi.atl.dataBase.jdbc.FavorisDao;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class FavorisRepositoryTest {

    @Mock
    private FavorisDao mock;

    private final FavorisDto parc;
    private final FavorisDto churchill;

    private final List<FavorisDto> all;

    public FavorisRepositoryTest() {
        System.out.println("==== FavorisDto Constructor =====");
        parc = new FavorisDto("NEW", "SQUAREPANTS", "CHAHED");
        churchill = new FavorisDto("N", "CHURCHILL", "CDS");
        all = new ArrayList<>();
        all.add(parc);
        all.add(churchill);
    }

    @BeforeEach
    void init() throws RepositoryException {
        System.out.println("==== BEFORE EACH =====");
        //Mock behaviour
        Mockito.lenient().when(mock.select(parc.getKey())).thenReturn(parc);
        Mockito.lenient().when(mock.select(churchill.getKey())).thenReturn(null);
        Mockito.lenient().when(mock.selectAll()).thenReturn(all);
        Mockito.lenient().when(mock.select(null)).thenThrow(RepositoryException.class);
    }

    @Test
    public void testGetExist() throws Exception {
        System.out.println("testGetExist");
        //Arrange
        FavorisDto expected = parc;
        FavorisRepository repository = new FavorisRepository(mock);
        //Action
        FavorisDto result = repository.get(parc.getKey());
        //Assert        
        assertEquals(expected, result);
        Mockito.verify(mock, times(1)).select(parc.getKey());
    }

    @Test
    public void testGetNotExist() throws Exception {
        System.out.println("testGetNotExist");
        //Arrange
        FavorisRepository repository = new FavorisRepository(mock);
        //Action
        FavorisDto result = repository.get(churchill.getKey());
        //Assert        
        assertNull(result);
        Mockito.verify(mock, times(1)).select(churchill.getKey());
    }

    @Test
    public void testGetIncorrectParameter() throws Exception {
        System.out.println("testGetIncorrectParameter");
        //Arrange
        FavorisRepository repository = new FavorisRepository(mock);
        String incorrect = null;
        //Assert
        assertThrows(RepositoryException.class, () -> {
            //Action
            repository.get(incorrect);
        });
        Mockito.verify(mock, times(1)).select(null);
    }

    @Test
    public void testGetAll() throws Exception {
        System.out.println("testGetFullStudentExist");
        //Arrange
        List<FavorisDto> expected = all;
        FavorisRepository repository = new FavorisRepository(mock);
        //Action
        List<FavorisDto> result = repository.getAll();
        //Assert        
        assertEquals(expected, result);
        Mockito.verify(mock, times(1)).selectAll();
    }
}
