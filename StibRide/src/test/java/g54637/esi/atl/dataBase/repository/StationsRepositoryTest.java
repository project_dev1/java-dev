package g54637.esi.atl.dataBase.repository;


import g54637.esi.atl.dataBase.dto.StationDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import g54637.esi.atl.dataBase.jdbc.StationDao;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class StationsRepositoryTest {

    private static final int KEY = 8032;
    private final StationDto parc;
    private final StationDto bervoets;
    private final List<StationDto> all;
    
    @Mock
    private StationDao mock;

    public StationsRepositoryTest() {
        System.out.println("StudentRepositoryTest Constructor");
        //Test data
        parc = new StationDto(KEY, "PARC");
        bervoets = new StationDto(99_999, "BERVOETS");
        all = new ArrayList<>();
        all.add(parc);
        all.add(bervoets);
    }

    @BeforeEach
    void init() throws RepositoryException {
        System.out.println("==== BEFORE EACH =====");
        //Mock behaviour
        Mockito.lenient().when(mock.select(parc.getKey())).thenReturn(parc);
        Mockito.lenient().when(mock.select(bervoets.getKey())).thenReturn(null);
        Mockito.lenient().when(mock.selectAll()).thenReturn(all);
        Mockito.lenient().when(mock.select(null)).thenThrow(RepositoryException.class);
    }

    @Test
    public void testGetExist() throws Exception {
        System.out.println("testGetExist");
        //Arrange
        StationDto expected = parc;
        StationRepository repository = new StationRepository(mock);
        //Action
        StationDto result = repository.get(KEY);
        //Assert        
        assertEquals(expected, result);
        Mockito.verify(mock, times(1)).select(KEY);
    }

    @Test
    public void testGetNotExist() throws Exception {
        System.out.println("testGetNotExist");
        //Arrange
        StationRepository repository = new StationRepository(mock);
        //Action
        StationDto result = repository.get(bervoets.getKey());
        //Assert        
        assertNull(result);
        Mockito.verify(mock, times(1)).select(bervoets.getKey());
    }

    @Test
    public void testGetIncorrectParameter() throws Exception {
        System.out.println("testGetIncorrectParameter");
        //Arrange
        StationRepository repository = new StationRepository(mock);
        Integer incorrect = null;
        //Assert
        assertThrows(RepositoryException.class, () -> {
            //Action
            repository.get(incorrect);
        });
        Mockito.verify(mock, times(1)).select(null);
    }

    @Test
    public void testGetAll() throws Exception {
        System.out.println("testGetFullStudentExist");
        //Arrange
        List<StationDto> expected = all;
        StationRepository repository = new StationRepository(mock);
        //Action
        List<StationDto> result = repository.getAll();
        //Assert        
        assertEquals(expected, result);
        Mockito.verify(mock, times(1)).selectAll();
    }
}
