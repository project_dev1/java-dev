package g54637.esi.atl.model.utils;

import g54637.esi.atl.model.Model;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public interface Observer {

    /**
     * update the observer
     *
     * @param model the model.
     */
    void update(Model model);

}
