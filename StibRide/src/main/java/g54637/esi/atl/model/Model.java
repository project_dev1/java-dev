package g54637.esi.atl.model;

import g54637.esi.atl.dataBase.dto.FavorisDto;
import g54637.esi.atl.dataBase.dto.StationDto;
import g54637.esi.atl.dataBase.dto.StopDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import g54637.esi.atl.dataBase.repository.FavorisRepository;
import g54637.esi.atl.dataBase.repository.StationRepository;
import g54637.esi.atl.dataBase.repository.StopRepository;
import g54637.esi.atl.model.utils.Observer;
import g54637.esi.atl.model.utils.Subject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class Model implements Subject {

    private final StationRepository station;
    private final StopRepository stop;
    private final FavorisRepository favoris;
    private final Set<Node> nodes = new HashSet<>();
    private Graph graph;
    private int count;

    public Model() throws RepositoryException, IOException {
        station = new StationRepository();
        stop = new StopRepository();
        favoris = new FavorisRepository();
        count = 0;
    }

    public List<StationDto> getStations() throws RepositoryException {
        return station.getAll();
    }

    public List<StopDto> getAdjacentesStation(StationDto station) throws RepositoryException {
        return stop.selectAdjacentesStation(station);
    }

    public String addFavoris(FavorisDto item) throws RepositoryException {
        return favoris.add(item);
    }

    public void removeFavoris(String key) throws RepositoryException {
        favoris.remove(key);
    }

    public void updateFavoris(FavorisDto old, FavorisDto item) throws RepositoryException {
        favoris.update(old, item);
    }

    /**
     * create the graph of station.
     *
     * @throws RepositoryException
     */
    public void createGraph() throws RepositoryException {
        graph = new Graph();
        nodes.clear();
        List< StationDto> stationList = getStations();
        for (StationDto stationDto : stationList) {
            nodes.add(new Node(stationDto));
        }
        List<StopDto> listAdj = new ArrayList<>();
        for (Node node : nodes) {
                listAdj = getAdjacentesStation(node.getStation());
            for (StopDto adj : listAdj) {
                for (Node noeud : nodes) {
                    if (noeud.getStation().getKey().equals(adj.getStation().getKey())) {
                        node.addDestination(noeud, 1);
                    }
                }
            }
            graph.addNode(node);
        }
       
    }

    /**
     * get the sorthed path of 2 station
     *
     * @param source the origine station.
     * @param destination the destination station
     * @return the list of the station between 2 station.
     * @throws RepositoryException
     */
    public List<StopDto> getShortedPath(StationDto source, StationDto destination)
            throws RepositoryException {
        List<StopDto> shortPath = new ArrayList<>();
        Graph graphSource = new Graph();
        count = 0;
        for (var node : nodes) {
            if (source.getKey().equals(node.getStation().getKey())) {
                graphSource = Dijkstra.calculateShortestPathFromSource(graph, node);
            }
        }
        for (var node : graphSource.getNodes()) {
            if (node.getStation().getKey().equals(destination.getKey())) {
                List<Node> pathNode = node.getShortestPath();
                for (Node node1 : pathNode) {
                    shortPath.add(stop.get(node1.getStation().getKey()));
                    count++;
                }
            }
        }
        shortPath.add(stop.get(destination.getKey()));
        count += 1;
        notifyObserver();
        return shortPath;
    }

    public int getCount() {
        return count;
    }

    /**
     * notifyObserver is a method for notify the observer
     */
    @Override
    public void notifyObserver() {
        observers.forEach((observer) -> {
            observer.update(this);
        });
    }

    /**
     * register is a method for add a new observer
     *
     * @param observer the observer
     */
    @Override
    public void register(Observer observer) {
        if (observer == null) {
            throw new IllegalArgumentException("a observer can't be null");
        }
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

}
