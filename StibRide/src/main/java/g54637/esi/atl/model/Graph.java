package g54637.esi.atl.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author baeldung.com
 */
public class Graph {

    private final Set<Node> nodes = new HashSet<>();

    public void addNode(Node nodeA) {
        nodes.add(nodeA);
    }

    public Set<Node> getNodes() {
        return nodes;
    }
    

    @Override
    public String toString() {
        return  nodes + "";
    }
    
}
