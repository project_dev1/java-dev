package g54637.esi.atl.model;

import g54637.esi.atl.dataBase.dto.LinesDto;
import g54637.esi.atl.dataBase.dto.StationDto;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author baeldung.com
 */
public class Node {

    private StationDto station;
    private Map<Node, Integer> adjacentNodes = new HashMap<>();
    private String name;
    private List<Node> shortestPath = new LinkedList<>();
    private Integer distance = Integer.MAX_VALUE;

    public void addDestination(Node destination, int distance) {
        adjacentNodes.put(destination, distance);
    }

    public Integer getDistance() {
        return distance;
    }

    public Node(StationDto station) {
        this.station = station;
        this.name = station.getName();
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Map<Node, Integer> getAdjacentNodes() {
        return adjacentNodes;
    }

    public void setAdjacentNodes(Map<Node, Integer> adjacentNodes) {
        this.adjacentNodes = adjacentNodes;
    }

    public StationDto getStation() {
        return station;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getShortestPath() {
        return shortestPath;
    }

    public void setShortestPath(List<Node> shortestPath) {
        this.shortestPath = shortestPath;
    }

    @Override
    public String toString() {
        return name;
    }

}
