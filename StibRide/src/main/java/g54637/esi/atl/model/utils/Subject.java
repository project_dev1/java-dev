package g54637.esi.atl.model.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public interface Subject {

    List<Observer> observers = new ArrayList<>();

    /**
     * register is a method for add a new observer
     *
     * @param observer the observer
     */
    void register(Observer observer);

    /**
     * notifyObserver is a method for notify the observer
     */
    void notifyObserver();

}
