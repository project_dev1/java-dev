package g54637.esi.atl.presenter;

import g54637.esi.atl.dataBase.dto.FavorisDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import g54637.esi.atl.model.Model;
import g54637.esi.atl.model.utils.Observer;
import g54637.esi.atl.view.View;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class Presenter implements Observer {

    private final View view;
    private final Model model;

    public Presenter(Model model, View view) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void update(Model model) {
        view.disableSearchDone();
        view.setNbStations(model.getCount());
    }

    public void buttonSearch() throws RepositoryException {
        model.createGraph();
        view.setAdjacenteStation(model.getShortedPath(view.getOriginStation(),
                view.getDestinationStation()));
        view.searchDone();
    }

    public void favorisAdd() throws RepositoryException {
        FavorisDto fav = new FavorisDto(view.alertGetNameFavoris(),
                view.getOrigineFavoris(), view.getDestinationFavoris());
        view.addFavoris(fav);
        model.addFavoris(fav);
    }

    public void favorisRemove() throws RepositoryException {
        FavorisDto fav = new FavorisDto(view.removeFavoris(), 
                view.getOrigineFavoris(), view.getDestinationFavoris());
        view.removeFavoris(fav);
        model.removeFavoris(fav.getKey());

    }

    public void favorisUpdate() throws RepositoryException {
        FavorisDto oldFav = new FavorisDto(view.updateFavoris(),
                view.getOrigineFavoris(), view.getDestinationFavoris());
        
        FavorisDto newfav = new FavorisDto(view.updateNewFavoris(),
                view.getOrigineFavoris(), view.getDestinationFavoris());
        view.removeFavoris(oldFav);
        view.addFavoris(newfav);
        model.updateFavoris(oldFav, newfav);
    }

    public void initialize() throws RepositoryException {
        view.setStationSource(model.getStations());
        view.setStationDestination(model.getStations());
    }

}
