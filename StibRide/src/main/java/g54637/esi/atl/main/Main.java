package g54637.esi.atl.main;

import g54637.atl.esi.dataBase.configµ.ConfigManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import g54637.esi.atl.model.Model;
import g54637.esi.atl.presenter.Presenter;
import g54637.esi.atl.view.View;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        ConfigManager.getInstance().load();
        String dbUrl = ConfigManager.getInstance().getProperties("db.url");
        System.out.println("Base de données stockée : " + dbUrl);
        stage.setTitle("HE2B ESI - STIB Planner");
        Model model = new Model();
        FXMLLoader loader
                = new FXMLLoader(getClass().getResource("/fxml/StibView.fxml"));
        Parent root = loader.load();
        View view = loader.getController();
        Presenter presenter = new Presenter(model, view);
        model.register(presenter);
        view.getAdjacentes(presenter);
        view.getFavoris(presenter);
        view.getUpdateFavoris(presenter);
        view.getRemoveFavoris(presenter);
        presenter.initialize();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

}
