package g54637.esi.atl.view;

import g54637.esi.atl.dataBase.exception.RepositoryException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import g54637.esi.atl.presenter.Presenter;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class FavorisRemoveHandler implements EventHandler<ActionEvent> {

    private final Presenter presenter;

    public FavorisRemoveHandler(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void handle(ActionEvent t) {
        try {
            presenter.favorisRemove();
        } catch (RepositoryException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
