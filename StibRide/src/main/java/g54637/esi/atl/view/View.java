package g54637.esi.atl.view;

import g54637.esi.atl.dataBase.dto.FavorisDto;
import g54637.esi.atl.dataBase.dto.StationDto;
import g54637.esi.atl.dataBase.dto.StopDto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import org.controlsfx.control.SearchableComboBox;
import g54637.esi.atl.presenter.Presenter;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class View implements Initializable {

    @FXML
    private SearchableComboBox<StationDto> origine;

    @FXML
    private SearchableComboBox<StationDto> destination;

    @FXML
    private TableView<StopDto> tableView;

    @FXML
    private TableView<FavorisDto> favorisTableView;
    
    @FXML
    private Label recherche;

    @FXML
    private Label nbStation;

    @FXML
    private TableColumn<StopDto, String> stationShort;

    @FXML
    private TableColumn<StopDto, String> lignesShort;

    @FXML
    private TableColumn<FavorisDto, String> name;

    @FXML
    private TableColumn<FavorisDto, String> origineFavoris;

    @FXML
    private TableColumn<FavorisDto, String> destinationFavoris;

    @FXML
    private Button search;

    @FXML
    private Button favoris;

    @FXML
    private Button removeFavoris;

    @FXML
    private Button updateFavoris;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        stationShort.setCellValueFactory(new PropertyValueFactory<>("NameStation"));
        lignesShort.setCellValueFactory(new PropertyValueFactory<>("IdLineTableView"));
        name.setCellValueFactory(new PropertyValueFactory<>("Key"));
        origineFavoris.setCellValueFactory(new PropertyValueFactory<>("Origin"));
        destinationFavoris.setCellValueFactory(new PropertyValueFactory<>("Destination"));
    }

    public String getOrigineFavoris() {
        return origine.getValue().getName();
    }

    public String getDestinationFavoris() {
        return destination.getValue().getName();
    }

    public void getAdjacentes(Presenter presenter) {
        SearchHandler handler = new SearchHandler(presenter);
        search.setOnAction(handler);
    }

    public void getUpdateFavoris(Presenter presenter) {
        FavorisUpdateHandler handler = new FavorisUpdateHandler(presenter);
        updateFavoris.setOnAction(handler);
    }

    public void getFavoris(Presenter presenter) {
        FavorisHandler handler = new FavorisHandler(presenter);
        favoris.setOnAction(handler);
    }

    public void getRemoveFavoris(Presenter presenter) {
        FavorisRemoveHandler handler = new FavorisRemoveHandler(presenter);
        removeFavoris.setOnAction(handler);
    }

    public String removeFavoris() {
        TextInputDialog inputdialog = new TextInputDialog("favorits to remove");
        inputdialog.setContentText("Name : ");
        inputdialog.setHeaderText("Name favoris");
        Optional<String> nameFav = inputdialog.showAndWait();
        return nameFav.get();
    }

    public String updateFavoris() {
        TextInputDialog inputdialog = new TextInputDialog("favoris to update");
        inputdialog.setContentText("Old Name : ");
        inputdialog.setHeaderText("Name favoris");
        Optional<String> newName = inputdialog.showAndWait();
        return newName.get();
    }

    public String updateNewFavoris() {
        TextInputDialog inputdialog1 = new TextInputDialog("favoris to update");
        inputdialog1.setContentText("New Name : ");
        inputdialog1.setHeaderText("Name favoris");
        Optional<String> nameFav = inputdialog1.showAndWait();
        return nameFav.get();
    }

    public String alertGetNameFavoris() {
        TextInputDialog inputdialog = new TextInputDialog("favoris name");
        inputdialog.setContentText("Name : ");
        inputdialog.setHeaderText("Name favoris");
        Optional<String> nameFav = inputdialog.showAndWait();
        return nameFav.get();
    }

    @FXML
    public void exitButtonAction(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    public void clearButtonAction(ActionEvent event) {
        origine.setValue(null);
        destination.setValue(null);
    }

    public StationDto getOriginStation() {
        return origine.getValue();
    }

    public StationDto getDestinationStation() {
        return destination.getValue();
    }

    public void setStationSource(List<StationDto> station) {
        origine.getItems().addAll(station);
    }

    public void setStationDestination(List<StationDto> station) {
        destination.getItems().addAll(station);
    }

    public void setAdjacenteStation(List<StopDto> adj) {
        tableView.getItems().clear();
        for (StopDto dto : adj) {
            tableView.getItems().add(dto);
        }
    }

    public void addFavoris(FavorisDto fav) {
        favorisTableView.getItems().add(fav);
    }

    public void removeFavoris(FavorisDto fav) {
        favorisTableView.getItems().remove(fav);
    }

    public void setNbStations(int count) {
        nbStation.setText("Nombre de stations : " + count);
    }

    public void searchDone() {
        recherche.setText("Recherche terminé");
    }

    public void disableSearchDone() {
        recherche.setText("");
    }

    @FXML
    public void helpButtonAction(ActionEvent event) {
        Alert a = new Alert(AlertType.INFORMATION);
        a.setTitle("Astuce-Aide");
        a.setHeaderText("Comment s'y retrouver?");
        a.setContentText("1.Selectionner la station d'origine\n"
                + "2. Selectionner la station de destination\n"
                + "\nL'application fera le necesaire pour vous trouver le"
                + " chemin le plus court");
        a.show();
    }
}
