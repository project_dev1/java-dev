package g54637.atl.esi.model;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public enum TypeThreads {

    BUBBLESORT, MERGESORT;

    @Override
    public String toString() {
        switch (this) {
            case BUBBLESORT -> {
                return "BubbleSort";
            }
            case MERGESORT -> {
                return "MergeSort";
            }
        }
        return null;
    }
}
