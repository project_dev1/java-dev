package g54637.atl.esi.model;

/**
 *
 * @author Baeldung 
 */
public class MergeSort implements Sort {

    @Override
    public long sort(int[] arr) {
        long count = 0;
        int n = arr.length;
        count++;
        if (n < 2) {
            count++;
            return count;
        }
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];
        count += 3;
        for (int i = 0; i < mid; i++) {
            l[i] = arr[i];
            count++;
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = arr[i];
            count++;
        }
        count += sort(l);
        count += sort(r);
        count += merge(arr, l, r, mid, n - mid);
        return count;
    }

    protected long merge(int[] a, int[] l, int[] r, int left, int right) {
        long count = 1;
        int i = 0, j = 0, k = 0;
        count += 3;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
                ++count;
            } else {
                a[k++] = r[j++];
                ++count;
            }
        }
        while (i < left) {
            a[k++] = l[i++];
            ++count;
        }
        while (j < right) {
            a[k++] = r[j++];
            ++count;
        }

        return count;
    }

}
