package g54637.atl.esi.model;

/**
 *
 * @author Baeldung 
 */
public class BubbleSort implements Sort {

    @Override
    public long sort(int[] arr) {
        long count = 0;
        int i = 0, n = arr.length;
        boolean swapNeeded = true;
        count += 3;
        while (i < n - 1 && swapNeeded) {
            swapNeeded = false;
            count++;
            for (int j = 1; j < n - i; j++) {
                if (arr[j - 1] > arr[j]) {
                    int temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                    swapNeeded = true;
                    count += 5;
                }
            }
            if (!swapNeeded) {
                count++;
                break;
            }
            count++;
            i++;
        }
        return count;
    }
    
}
