package g54637.atl.esi.model;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public enum LevelThreads {
    
    VERYEASY, EASY, MEDIUM, HARD;

    @Override
    public String toString() {
        switch (this) {
            case VERYEASY -> {
                return "Very Easy : 100";
            }
            case EASY -> {
                return "Easy : 1000";
            }
            case MEDIUM -> {
                return "Medium : 10 000";
            }
            case HARD -> {
                return "Hard : 100 000";
            }
        }
        return null;
    }

    public int getLevel() {
        switch (this) {
            case VERYEASY -> {
                return 100;
            }
            case EASY -> {
                return 1000;
            }
            case MEDIUM -> {
                return 10000;
            }
            case HARD -> {
                return 100000;
            }
        }
        return 0;
    }
}
