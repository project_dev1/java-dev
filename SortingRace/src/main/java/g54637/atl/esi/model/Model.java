package g54637.atl.esi.model;

import g54637.esi.atl.dataBase.dto.SimulationDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import g54637.esi.atl.dataBase.repository.SimulationRepository;
import java.util.ArrayList;
import java.util.List;
import g54637.esi.atl.model.utils.Observer;


/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class Model {

    private final List<Thread> sorting;
    private DataThread dataSet;
            private final SimulationRepository simulationRepository;

    
    public Model(int nbThread, TypeThreads type, int size,Observer observer) throws RepositoryException {
        JobManager job = new JobManager(size);
        sorting = new ArrayList<>();
        for (int i = 0; i < nbThread; i++) {
            SortingMyThread sort = new SortingMyThread(job, type);
            sort.register(observer);
            sorting.add(new Thread(sort));
            
        }
       simulationRepository=new SimulationRepository();
        sorting.forEach(t->t.start());
    }
   
  

    public List<SimulationDto> getStations() throws RepositoryException {
        return simulationRepository.getAll();
    }

    public Integer addSimulation(SimulationDto item) throws RepositoryException {
        return simulationRepository.add(item);
    }
    
}
