package g54637.atl.esi.model;

import java.util.Random;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class JobManager {

    
    private final int taille;
    private int incrementTabSize;
    private static final Random r = new Random();

    public JobManager(int taille) {
        this.taille = taille;
        this.incrementTabSize = 0;
    }

    public synchronized int[] generateTab() {
        int[] tab = new int[incrementTabSize];
        for (int i = 0; i < tab.length; i++) {
            tab[i] = r.nextInt(100);
        }
        return tab;
    }

    public synchronized int[] getNextTab() {
        if (incrementTabSize <= taille) {
            int[] tab = generateTab();
            incrementTabSize += taille / 10;
            return tab;
        }
        return null;
    }

    public int getIncrementSize() {
        return incrementTabSize;
    }


}
