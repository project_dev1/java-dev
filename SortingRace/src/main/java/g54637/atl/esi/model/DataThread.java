package g54637.atl.esi.model;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class DataThread {
  
   private final TypeThreads type; 
   private final int size;
   private final long operations;
   private final long duration;

    public DataThread(TypeThreads type, int size, long operations, long duration) {
        this.type = type;
        this.size = size;
        this.operations = operations;
        this.duration = duration;
    }

    public TypeThreads getType() {
        return type;
    }

    public int getSize() {
        return size;
    }

    public long getOperations() {
        return operations;
    }

    public long getDuration() {
        return duration;
    }
   
}
