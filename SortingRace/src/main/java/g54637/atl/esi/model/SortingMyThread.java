package g54637.atl.esi.model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import g54637.esi.atl.model.utils.Observable;
import g54637.esi.atl.model.utils.Observer;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class SortingMyThread implements Runnable, Observable { 

    private final JobManager job;
    private final Sort sort;
    private LocalDateTime debutTri;
    private LocalDateTime finTri;
    private final TypeThreads type;
    private int[] tab;
    private long count;
    private final List<Observer> observers = new ArrayList<>();

    public SortingMyThread(JobManager job, TypeThreads type) {
        this.type = type;
        if (type == TypeThreads.BUBBLESORT) {
            sort = new BubbleSort();
        } else {
            sort = new MergeSort();
        }
        this.job = job;
    }

    @Override
    public void run() {
        tab = job.getNextTab();
        while (tab != null) {
            count = 0;
            debutTri = LocalDateTime.now();
            count += sort.sort(tab);
            finTri = LocalDateTime.now();
            notifyObserver();
            tab = job.getNextTab();
        }
    }

    protected void notifyObserver() {
        DataThread setThread = new DataThread(type, tab.length, count,
                Duration.between(debutTri, finTri).toMillis());
        observers.forEach(o -> o.update(setThread));
    }

    @Override
    public void register(Observer observer) {
        observers.add(observer);

    }

    @Override
    public void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

}
