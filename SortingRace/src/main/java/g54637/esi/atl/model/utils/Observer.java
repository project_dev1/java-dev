package g54637.esi.atl.model.utils;

import g54637.atl.esi.model.DataThread;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public interface Observer {

    /*
     * update the observer
     */
    void update(DataThread setThread);
}
