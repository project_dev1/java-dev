package g54637.esi.atl.model.utils;

/**
 * the subject
 *
 * @author Billal Zidi
 */
public interface Observable {

    // elle est devenue interface car le comportement du notify a changer car on devait le redefinir
    
    /**
     * register is a method for add a new observer
     *
     * @param observer the observer
     */
    public void register(Observer observer);

    public void deleteObserver(Observer observer);

}
