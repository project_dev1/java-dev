package g54637.esi.atl.dataBase.dto;


import g54637.atl.esi.model.TypeThreads;
import java.sql.Date;
import java.time.LocalDateTime;


public class SimulationDto extends Dto<Integer> {

    private final String timestamp;
    private final String sort_type;
    private final Integer max_size;

    public SimulationDto(Integer key,String timestamp, String sort_type, Integer 
            max_size ) {
        super(key);
        this.timestamp = timestamp;
        this.sort_type = sort_type;
        this.max_size = max_size;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getSort_type() {
        return sort_type;
    }

    public Integer getMax_size() {
        return max_size;
    }
}