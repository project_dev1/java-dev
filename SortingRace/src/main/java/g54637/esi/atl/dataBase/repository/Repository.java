package g54637.esi.atl.dataBase.repository;

import g54637.esi.atl.dataBase.dto.Dto;
import g54637.esi.atl.dataBase.exception.RepositoryException;

import java.util.List;

/**
 * @param <K>
 * @param <T>
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public interface Repository<K, T extends Dto<K>> {

    /**
     * Returns all the elements of the repository.
     *
     * @return all the elements of the repository.
     * @throws RepositoryException if the repository can't access to the
     * elements.
     */
    List<T> getAll() throws RepositoryException;

    /**
     * Return the element of the repository with the specific key.
     *
     * @param key
     * @return the element of the repository with the specific key.
     * @throws RepositoryException if the repository can't access to the
     * element.
     */
    T get(K key) throws RepositoryException;

    /**
     * Returns true if the element exist in the repository and false otherwise.
     * An element is found by this key.
     *
     * @param key key of the element.
     * @return true if the element exist in the repository and false otherwise.
     * @throws RepositoryException if the repository can't access to the
     * element.
     */
    boolean contains(K key) throws RepositoryException;

    /**
     * Add an element to the repository.If the element exists, the repository
     * updates this element.
     *
     * @param item the element to add.
     * @return the element's key, usefull when the key is auto-generated.
     * @throws RepositoryException if the repository can't access to the
     * element.
     */
    K add(T item) throws RepositoryException;

    /**
     * Removes the element of the specific key.
     *
     * @param key key of the element to removes.
     * @throws RepositoryException if the repository can't access to the
     * element.
     */
    void remove(K key) throws RepositoryException;

    /**
     * Removes the element of the specific key.
     *
     * @param old
     * @param item key of the element to removes.
     * @throws RepositoryException if the repository can't access to the
     * element.
     */
    void update(T old ,T item) throws RepositoryException;
}
