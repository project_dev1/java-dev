package g54637.esi.atl.dataBase.jdbc;

import g54637.esi.atl.dataBase.dto.SimulationDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import g54637.esi.atl.dataBase.repository.Dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class SimulationDao implements Dao<Integer, SimulationDto> {

    private final Connection connexion;

    private SimulationDao() throws RepositoryException {
        connexion = DBManager.getInstance().getConnection();
    }

    public static SimulationDao getInstance() throws RepositoryException {
        return SimulationDaoHolder.getInstance();
    }

    @Override
    public List<SimulationDto> selectAll() throws RepositoryException {
        String sql = "SELECT id , timestamp , sort_type ,max_size FROM SIMULATION";
        List<SimulationDto> simulationDto = new ArrayList<>();
        try ( Statement stmt = connexion.createStatement();  ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                SimulationDto dto = new SimulationDto(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
                simulationDto.add(dto);
            }
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
        return simulationDto;
    }

    @Override
    public SimulationDto select(Integer key) throws RepositoryException {
        if (key == null) {
            throw new RepositoryException("Aucune clé donnée en paramètre");
        }
       String sql = "SELECT id , timestamp , sort_type ,max_size FROM SIMULATION WHERE id = ?";
        SimulationDto dto = null;
        try ( PreparedStatement pstmt = connexion.prepareStatement(sql)) {
            pstmt.setInt(1, key);
            ResultSet rs = pstmt.executeQuery();
            int count = 0;
            while (rs.next()) {
                dto = new SimulationDto(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
                count++;
            }
            if (count > 1) {
                throw new RepositoryException("Record pas unique " + key);
            }
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
        return dto;
    }

    @Override
    public Integer insert(SimulationDto item) throws RepositoryException {
    if (item == null) {
            throw new RepositoryException("Aucune élément donné en paramètre");
        }
        String sql = "INSERT INTO SIMULATION(id,timestamp,sort_type,max_size) values(?, ?, ? ,?)";
        try ( PreparedStatement pstmt = connexion.prepareStatement(sql)) {
            pstmt.setInt(1, item.getKey());
            pstmt.setString(2, item.getTimestamp());
            pstmt.setString(3, item.getSort_type());
            pstmt.setInt(4, item.getMax_size());
            pstmt.execute();
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
        return item.getKey();
    }

    @Override
    public void delete(Integer key) throws RepositoryException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(SimulationDto oldItem, SimulationDto key) throws RepositoryException {
        throw new UnsupportedOperationException();
    }

    private static class SimulationDaoHolder {

        private static SimulationDao getInstance() throws RepositoryException {
            return new SimulationDao();
        }
    }
}
