package g54637.esi.atl.dataBase.dto;

import java.util.Objects;

/**
 * @param <K> The key.
 * @author Billal Zidi <54637@etu.he2b.be>
 */

public class Dto<K> {

    protected K key;

    protected Dto(K key) {
        if (key == null) {
            throw new IllegalArgumentException("Lines missing " + key);
        }
        this.key = key;
    }

    public K getKey() {
        return key;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.key);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dto<?> other = (Dto<?>) obj;
        return Objects.equals(this.key, other.key);
    }

    @Override
    public String toString() {
        return "" + key;
    }

}
