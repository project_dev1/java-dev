package g54637.esi.atl.dataBase.repository;

import g54637.esi.atl.dataBase.dto.SimulationDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import g54637.esi.atl.dataBase.jdbc.SimulationDao;

import java.util.List;

/**
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class SimulationRepository implements Repository<Integer, SimulationDto> {

    private final SimulationDao simulationDao;

    public SimulationRepository() throws RepositoryException {
        simulationDao = SimulationDao.getInstance();
    }

    SimulationRepository(SimulationDao simulationDao) {
        this.simulationDao = simulationDao;
    }

    @Override
    public List<SimulationDto> getAll() throws RepositoryException {
        return simulationDao.selectAll();
    }

    @Override
    public SimulationDto get(Integer key) throws RepositoryException {
        return simulationDao.select(key);
    }

    @Override
    public boolean contains(Integer key) throws RepositoryException {
        SimulationDto refreshItem = simulationDao.select(key);
        return refreshItem != null;
    }

    @Override
    public Integer add(SimulationDto item) throws RepositoryException {
      return simulationDao.insert(item);
    }

    @Override
    public void remove(Integer key) throws RepositoryException {
        simulationDao.delete(key);
    }

    @Override
    public void update(SimulationDto olditem ,SimulationDto item) throws RepositoryException {
        simulationDao.update( olditem ,item);
    }

}
