package g54637.esi.atl.view;

import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import g54637.atl.esi.model.DataThread;
import g54637.atl.esi.model.LevelThreads;
import g54637.atl.esi.model.Model;
import g54637.atl.esi.model.TypeThreads;
import g54637.esi.atl.dataBase.dto.SimulationDto;
import g54637.esi.atl.dataBase.exception.RepositoryException;
import g54637.esi.atl.model.utils.Observer;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class View implements Initializable, Observer {

    @FXML
    private ChoiceBox<TypeThreads> sortChoice;

    @FXML
    private Spinner<Integer> threadSpinner;

    @FXML
    private ChoiceBox<LevelThreads> configurationChoice;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private Button start;

    @FXML
    private Label leftStatus;

    @FXML
    private Label rightStatus;

    @FXML
    private LineChart chart;

    @FXML
    private TableView<DataThread> table;

    @FXML
    public TableColumn<DataThread, TypeThreads> nameCol;

    @FXML
    public TableColumn<DataThread, Integer> sizeCol;

    @FXML
    public TableColumn<DataThread, Long> swapCol;

    @FXML
    public TableColumn<DataThread, Long> durationCol;

    private final XYChart.Series bubbleSort = new XYChart.Series();

    private final XYChart.Series mergeSort = new XYChart.Series();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        sortChoice.getItems().addAll(TypeThreads.values());
        configurationChoice.getItems().addAll(LevelThreads.values());
        sortChoice.getSelectionModel().select(0);
        configurationChoice.getSelectionModel().select(0);
        threadSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10));
        LocalDateTime timeBefore = LocalDateTime.now();
        LocalDateTime timeAfter = LocalDateTime.now();
        Duration result = Duration.between(timeBefore, timeAfter);
        DateTimeFormatter form = DateTimeFormatter.ofPattern(("HH::mm:ss.SSS"));
        rightStatus.setText("Derniere exécution | Début : "
                + timeBefore.format(form) + " - " + "Fin : "
                + timeAfter.format(form)
                + " Durée : Duration :" + result.toMillis() + " ms ");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        sizeCol.setCellValueFactory(new PropertyValueFactory<>("size"));
        swapCol.setCellValueFactory(new PropertyValueFactory<>("operations"));
        durationCol.setCellValueFactory(new PropertyValueFactory<>("duration"));
        table.getColumns().setAll(nameCol, sizeCol, swapCol, durationCol);
        createChart();
    }

    private LineChart createChart() {
        bubbleSort.setName(TypeThreads.BUBBLESORT.toString());
        mergeSort.setName(TypeThreads.MERGESORT.toString());
        chart.getData().addAll(bubbleSort, mergeSort);
        return chart;
    }

    @FXML
    public void startButton(ActionEvent e) throws RepositoryException {
        LevelThreads levelSize = configurationChoice.getValue();
        TypeThreads type = sortChoice.getValue();
        int nbThread = threadSpinner.getValue();
        Model model = new Model(nbThread, type, levelSize.getLevel(), this);
        if (type == TypeThreads.BUBBLESORT) {
            bubbleSort.getData().clear();
            SimulationDto simulation = new SimulationDto(3,"on est bon",type.toString(),nbThread);
            model.addSimulation(simulation);
        } else {
            mergeSort.getData().clear();
            SimulationDto simulation = new SimulationDto(3,"on est bon",type.toString(),nbThread);
            model.addSimulation(simulation);
        }
    }

    @Override
    public void update(DataThread setThread) {
        Platform.runLater(() -> {
            table.getItems().add(setThread);
            if (setThread.getType() == TypeThreads.BUBBLESORT) {
                progressBar.setProgress(bubbleSort.getData().size() / 10.);
                bubbleSort.getData().add(new XYChart.Data<>(setThread.getSize(),
                        setThread.getOperations()));
                leftStatus.setText("Threads actifs : " + Thread.activeCount());
            } else {
                mergeSort.getData().add(new XYChart.Data<>(setThread.getSize(),
                        setThread.getOperations()));
                progressBar.setProgress(mergeSort.getData().size() / 10.);
                leftStatus.setText("Threads actifs : " + Thread.activeCount());

            }
        });

    }

}
