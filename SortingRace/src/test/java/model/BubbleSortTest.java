package model;

import g54637.atl.esi.model.BubbleSort;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Billal Zidi <54637@etu.he2b.be>
 */
public class BubbleSortTest {

    public BubbleSortTest() {
    }

    @Test
    public void whenSortedWithBubbleSort_thenGetSortedArray() {
        int[] array = {2, 1, 4, 6, 3, 5};
        int[] sortedArray = {1, 2, 3, 4, 5, 6};
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(array);        
        assertArrayEquals(array, sortedArray);
    }

    @Test
    public void whenAlwaysIsSort() {
        int[] array = {1, 2, 3, 4, 5, 6};
        int[] sortedArray = {1, 2, 3, 4, 5, 6};
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(array);
        assertArrayEquals(array, sortedArray);
    }

    @Test
    public void whenThereIsDoublon() {
        int[] array = {2, 1, 2, 1, 3, 3, 14, 14, 54, 54};
        int[] sortedArray = {1, 1, 2, 2, 3, 3, 14, 14, 54, 54};
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(array);
        assertArrayEquals(array, sortedArray);
    }

    @Test
    public void whenNumberAreNull() {
        int[] array = {0, 0, 0, 0, 0, 0};
        int[] sortedArray = {0, 0, 0, 0, 0, 0};
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(array);
        assertArrayEquals(array, sortedArray);
    }

    @Test
    public void whenWeHaveNegatifNumber() {
        int[] array = {-2, -1, 4, 16, -3, 5};
        int[] sortedArray = {-3, -2, -1, 4, 5, 16};
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(array);
        assertArrayEquals(array, sortedArray);
    }

    @Test
    public void whenWeHaveNegatifNumberAndNull() {
        int[] array = {-2, -1, 4, 16,0, -3, 5,0};
        int[] sortedArray = {-3, -2, -1,0,0, 4, 5, 16};
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(array);
        assertArrayEquals(array, sortedArray);
    }
    
    
     @Test
    public void whenWeHaveNegatifNumberAndPositive() {
        int[] array = {-2, -1, 4, 16,5, -3, 5,5};
        int[] sortedArray = {-3, -2, -1, 4,5,5, 5, 16};
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.sort(array);
        assertArrayEquals(array, sortedArray);
    }
    
}
